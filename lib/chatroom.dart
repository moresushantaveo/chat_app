import 'package:chat_app/main.dart';
import 'package:flutter/material.dart';
import 'package:melos_chat/melos_chat.dart';

class ChatRoom extends StatefulWidget {
  final Chat chat;
  const ChatRoom({
    Key? key,
    required this.chat,
  }) : super(key: key);

  @override
  State<ChatRoom> createState() => _ChatRoomState();
}

class _ChatRoomState extends State<ChatRoom> {
  late List<Message> conversation = [];
  TextEditingController messageController = TextEditingController();

  @override
  void initState() {
    super.initState();
  }

  Future<List<Message>> getConversation() {
    return firebaseChat.getAllChatConversation(widget.chat.chatId,
        descending: true);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey,
      appBar: AppBar(title: Text(widget.chat.username)),
      body: FutureBuilder(
        future: getConversation(),
        builder: (context, AsyncSnapshot<List<Message>> snapshot) {
          if (snapshot.connectionState == ConnectionState.done &&
              snapshot.hasData) {
            return Column(
              mainAxisSize: MainAxisSize.max,
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                Expanded(
                  child: ListView.builder(
                    shrinkWrap: true,
                    reverse: true,
                    itemCount: snapshot.data?.length,
                    itemBuilder: (context, index) {
                      return MessageTile(
                          message: snapshot.data![index].message,
                          isMessageSent: snapshot.data![index].sentBy ==
                              MelosChat.instance.uniqueUserId,
                          receivedMessageTileColor: Colors.blueGrey,
                          receivedMessageColor: Colors.white,
                          timestamp: snapshot.data![index].timestamp);
                    },
                  ),
                ),
              ],
            );
          }
          return const Center(
            child: CircularProgressIndicator(),
          );
        },
      ),
      bottomNavigationBar: SafeArea(
        child: Container(
          margin: const EdgeInsets.symmetric(horizontal: 8.0, vertical: 8.0),
          padding: const EdgeInsets.all(0.0),
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(34.0),
          ),
          child: ListTile(
            dense: true,
            minVerticalPadding: 0,
            minLeadingWidth: 0,
            title: TextFormField(
              controller: messageController,
              maxLines: null,
              decoration: const InputDecoration(
                hintText: "Message",
                focusedBorder: OutlineInputBorder(borderSide: BorderSide.none),
                enabledBorder: OutlineInputBorder(borderSide: BorderSide.none),
              ),
            ),
            trailing: IconButton(
              icon: const Icon(Icons.send, color: Colors.blue),
              onPressed: () async {
                if (messageController.text.trim().isNotEmpty) {
                  try {
                    await firebaseChat.sendMessage(
                        widget.chat.chatId,
                        Message(
                          message: messageController.text.trim(),
                          sentBy: MelosChat.instance.uniqueUserId,
                        ));
                  } catch (e) {
                    print(e);
                  } finally {
                    messageController.clear();
                    setState(() {});
                  }
                }
              },
            ),
          ),
        ),
      ),
    );
  }
}

// Widget MessageTimestampText({String? timestamp}) {
//   return Text(
//     timeago.format(DateTime.parse(timestamp!).toUtc().toLocal(),
//         locale: 'en_short'),
//     style: const TextStyle(fontSize: 12.0),
//   );
// }
