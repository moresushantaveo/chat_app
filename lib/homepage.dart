import 'package:chat_app/chatroom.dart';
import 'package:chat_app/main.dart';
import 'package:flutter/material.dart';
import 'package:melos_chat/melos_chat.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  void initState() {
    super.initState();
  }

  Future<List<Chat>> getData() async {
    return await firebaseChat.getAllMyChats(MelosChat.instance.uniqueUserId);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Chat App"),
      ),
      body: LayoutBuilder(
        builder: (context, constraints) => Container(
          constraints: constraints,
          child: FutureBuilder(
            future: getData(),
            builder: (ctx, AsyncSnapshot<List<Chat>> snap) => ListView(
              children: [
                ...List.generate(
                  snap.data!.length,
                  (index) {
                    return ListTile(
                      onTap: () {
                        Navigator.of(context).push(MaterialPageRoute(
                          builder: (context) =>
                              ChatRoom(chat: snap.data![index]),
                        ));
                      },
                      title: Text(snap.data![index].username),
                    );
                  },
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
