import 'package:chat_app/firebase_options.dart';
import 'package:chat_app/homepage.dart';
import 'package:firebase_chat/firebase_chat.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:melos_chat/melos_chat.dart';

late FirebaseChatService firebaseChat;

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp(
    options: DefaultFirebaseOptions.currentPlatform,
  );
  firebaseChat = MelosChat(
    service: ChatServices.FIREBASE,
    userId: 101.toString(),
  ).getService<FirebaseChatService>();
  runApp(const MainApp());
}

class MainApp extends StatelessWidget {
  const MainApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      home: HomePage(),
    );
  }
}
